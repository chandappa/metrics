package metrics

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// Init initializes the passed in metrics and initializes its fields
// using the passed in metric data.
//
// It uses reflection to initialize a struct containing metrics fields
// by assigning new Counter/Gauge/Timer values with the metric name retrieved
// from the `metric` tag and stats tags retrieved from the `tags` tag.
//
// Note: all fields of the struct must be exported, have a `metric` tag, and be
// of type Counter or Gauge or Timer.
//
func Init(m interface{}, data Data, globalTags map[string]string) error {
	// Allow user to opt out of reporting metrics by passing in nil.
	if data == nil {
		data = EmptyData
	}

	counterPtrType := reflect.TypeOf((*Counter)(nil)).Elem()
	gaugePtrType := reflect.TypeOf((*Gauge)(nil)).Elem()
	timerPtrType := reflect.TypeOf((*Timer)(nil)).Elem()
	histogramPtrType := reflect.TypeOf((*Histogram)(nil)).Elem()

	v := reflect.ValueOf(m).Elem()
	t := v.Type()
	for i := 0; i < t.NumField(); i++ {
		tags := make(map[string]string)
		for k, v := range globalTags {
			tags[k] = v
		}
		var buckets []float64
		field := t.Field(i)
		metric := field.Tag.Get("metric")
		if metric == "" {
			return fmt.Errorf("Field %s is missing a tag 'metric'", field.Name)
		}
		if tagString := field.Tag.Get("tags"); tagString != "" {
			tagPairs := strings.Split(tagString, ",")
			for _, tagPair := range tagPairs {
				tag := strings.Split(tagPair, "=")
				if len(tag) != 2 {
					return fmt.Errorf(
						"Field [%s]: Tag [%s] is not of the form key=value in 'tags' string [%s]",
						field.Name, tagPair, tagString)
				}
				tags[tag[0]] = tag[1]
			}
		}
		if bucketString := field.Tag.Get("buckets"); bucketString != "" {
			if field.Type.AssignableTo(timerPtrType) {
				return fmt.Errorf(
					"Field [%s]: Buckets are not currently initialized for timer metrics",
					field.Name)
			} else if field.Type.AssignableTo(histogramPtrType) {
				bucketValues := strings.Split(bucketString, ",")
				for _, bucket := range bucketValues {
					b, err := strconv.ParseFloat(bucket, 64)
					if err != nil {
						return fmt.Errorf(
							"Field [%s]: Bucket [%s] could not be converted to float64 in 'buckets' string [%s]",
							field.Name, bucket, bucketString)
					}
					buckets = append(buckets, b)
				}
			} else {
				return fmt.Errorf(
					"Field [%s]: Buckets should only be defined for Timer and Histogram metric types",
					field.Name)
			}
		}
		help := field.Tag.Get("help")
		var obj interface{}
		if field.Type.AssignableTo(counterPtrType) {
			obj = data.Counter(Options{
				Name: metric,
				Tags: tags,
				Help: help,
			})
		} else if field.Type.AssignableTo(gaugePtrType) {
			obj = data.Gauge(Options{
				Name: metric,
				Tags: tags,
				Help: help,
			})
		} else if field.Type.AssignableTo(timerPtrType) {
			obj = data.Timer(TimerOptions{
				Name: metric,
				Tags: tags,
				Help: help,
			})
		} else if field.Type.AssignableTo(histogramPtrType) {
			obj = data.Histogram(HistogramOptions{
				Name:    metric,
				Tags:    tags,
				Help:    help,
				Buckets: buckets,
			})
		} else {
			return fmt.Errorf(
				"Field %s is not a pointer to timer, gauge, or counter",
				field.Name)
		}
		v.Field(i).Set(reflect.ValueOf(obj))
	}
	return nil
}

// NSOptions defines the name and tags map associated with a metric data namespace
type NSOptions struct {
	Name string
	Tags map[string]string
}

// Options defines the information associated with a metric
type Options struct {
	Name string
	Tags map[string]string
	Help string
}

// TimerOptions defines the information associated with a metric
type TimerOptions struct {
	Name    string
	Tags    map[string]string
	Help    string
	Buckets []time.Duration
}

// HistogramOptions defines the information associated with a metric
type HistogramOptions struct {
	Name    string
	Tags    map[string]string
	Help    string
	Buckets []float64
}

// Data creates new metrics
type Data interface {
	Counter(metric Options) Counter
	Timer(metric TimerOptions) Timer
	Gauge(metric Options) Gauge
	Histogram(metric HistogramOptions) Histogram

	// Namespace returns a nested metrics data.
	Namespace(scope NSOptions) Data
}

// EmptyData is a metrics data that returns EmptyCounter, EmptyTimer, and EmptyGauge.
var EmptyData Data = emptyData{}

type emptyData struct{}

func (emptyData) Counter(options Options) Counter {
	return EmptyCounter
}

func (emptyData) Timer(options TimerOptions) Timer {
	return EmptyTimer
}

func (emptyData) Gauge(options Options) Gauge {
	return EmptyGauge
}

func (emptyData) Histogram(options HistogramOptions) Histogram {
	return EmptyHistogram
}

func (emptyData) Namespace(scope NSOptions) Data { return EmptyData }

// Gauge returns instantaneous measurements of something as an int64 value
type Gauge interface {
	// Update the gauge to the value passed in.
	Update(int64)
}

// EmptyGauge gauge that does nothing
var EmptyGauge Gauge = emptyGauge{}

type emptyGauge struct{}

func (emptyGauge) Update(int64) {}

// Counter tracks the number of times an event has occurred
type Counter interface {
	// Inc adds the given value to the counter.
	Inc(int64)
}

// EmptyCounter counter that does nothing
var EmptyCounter Counter = emptyCounter{}

type emptyCounter struct{}

func (emptyCounter) Inc(int64) {}

// Histogram that keeps track of a distribution of values.
type Histogram interface {
	// Records the value passed in.
	Record(float64)
}

// EmptyHistogram that does nothing
var EmptyHistogram Histogram = emptyHistogram{}

type emptyHistogram struct{}

func (emptyHistogram) Record(float64) {}

// Timer accumulates observations about how long some operation took,
// and also maintains a historgam of percentiles.
type Timer interface {
	// Records the time passed in.
	Record(time.Duration)
}

// EmptyTimer timer that does nothing
var EmptyTimer Timer = emptyTimer{}

type emptyTimer struct{}

func (emptyTimer) Record(time.Duration) {}

// StartStopwatch begins recording the executing time of an event, returning
// a Stopwatch that should be used to stop the recording the time for
// that event.  Multiple events can be occurring simultaneously each
// represented by different active Stopwatches
func StartStopwatch(timer Timer) Stopwatch {
	return Stopwatch{t: timer, start: time.Now()}
}

// A Stopwatch tracks the execution time of a specific event
type Stopwatch struct {
	t     Timer
	start time.Time
}

// Stop stops executing of the stopwatch and records the amount of elapsed time
func (s Stopwatch) Stop() {
	s.t.Record(s.ElapsedTime())
}

// ElapsedTime returns the amount of elapsed time (in time.Duration)
func (s Stopwatch) ElapsedTime() time.Duration {
	return time.Since(s.start)
}

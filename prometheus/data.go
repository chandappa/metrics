package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/chandappa/metrics"
	"sort"
	"strings"
	"time"
)

// Data implements metrics.Data backed by Prometheus registry.
type Data struct {
	scope      string
	tags       map[string]string
	cache      *vectorCache
	buckets    []float64
	normalizer *strings.Replacer
	separator  Separator
}

type options struct {
	registerer prometheus.Registerer
	buckets    []float64
	separator  Separator
}

// Separator represents the namespace separator to use
type Separator rune

const (
	// SeparatorUnderscore uses an underscore as separator
	SeparatorUnderscore Separator = '_'

	// SeparatorColon uses a colon as separator
	SeparatorColon = ':'
)

// Option is a function that sets some option for the Data constructor.
type Option func(*options)

// WithRegisterer returns an option that sets the registerer.
// If not used we fallback to prometheus.DefaultRegisterer.
func WithRegisterer(registerer prometheus.Registerer) Option {
	return func(opts *options) {
		opts.registerer = registerer
	}
}

// WithBuckets returns an option that sets the default buckets for histogram.
// If not used, we fallback to default Prometheus buckets.
func WithBuckets(buckets []float64) Option {
	return func(opts *options) {
		opts.buckets = buckets
	}
}

// WithSeparator returns an option that sets the default separator for the namespace
// If not used, we fallback to underscore.
func WithSeparator(separator Separator) Option {
	return func(opts *options) {
		opts.separator = separator
	}
}

func applyOptions(opts []Option) *options {
	options := new(options)
	for _, o := range opts {
		o(options)
	}
	if options.registerer == nil {
		options.registerer = prometheus.DefaultRegisterer
	}
	if options.separator == '\x00' {
		options.separator = SeparatorUnderscore
	}
	return options
}

// New creates a Data backed by Prometheus registry.
// Typically the first argument should be prometheus.DefaultRegisterer.
//
// Parameter buckets defines the buckets into which Timer observations are counted.
// Each element in the slice is the upper inclusive bound of a bucket. The
// values must be sorted in strictly increasing order. There is no need
// to add a highest bucket with +Inf bound, it will be added
// implicitly. The default value is prometheus.DefBuckets.
func New(opts ...Option) *Data {
	options := applyOptions(opts)
	return newData(
		&Data{ // dummy struct to be discarded
			cache:      newVectorCache(options.registerer),
			buckets:    options.buckets,
			normalizer: strings.NewReplacer(".", "_", "-", "_"),
			separator:  options.separator,
		},
		"",  // scope
		nil) // tags
}

func newData(parent *Data, scope string, tags map[string]string) *Data {
	return &Data{
		cache:      parent.cache,
		buckets:    parent.buckets,
		normalizer: parent.normalizer,
		separator:  parent.separator,
		scope:      scope,
		tags:       tags,
	}
}

// Counter implements Counter of metrics.Data.
func (d *Data) Counter(options metrics.Options) metrics.Counter {
	help := strings.TrimSpace(options.Help)
	if len(help) == 0 {
		help = options.Name
	}
	name := counterNamingConvention(d.subScope(options.Name))
	tags := d.mergeTags(options.Tags)
	labelNames := d.tagNames(tags)
	opts := prometheus.CounterOpts{
		Name: name,
		Help: help,
	}
	cv := d.cache.getOrMakeCounterVec(opts, labelNames)
	return &counter{
		counter: cv.WithLabelValues(d.tagsAsLabelValues(labelNames, tags)...),
	}
}

// Gauge implements Gauge of metrics.Data.
func (d *Data) Gauge(options metrics.Options) metrics.Gauge {
	help := strings.TrimSpace(options.Help)
	if len(help) == 0 {
		help = options.Name
	}
	name := d.subScope(options.Name)
	tags := d.mergeTags(options.Tags)
	labelNames := d.tagNames(tags)
	opts := prometheus.GaugeOpts{
		Name: name,
		Help: help,
	}
	gv := d.cache.getOrMakeGaugeVec(opts, labelNames)
	return &gauge{
		gauge: gv.WithLabelValues(d.tagsAsLabelValues(labelNames, tags)...),
	}
}

// Timer implements Timer of metrics.Data.
func (d *Data) Timer(options metrics.TimerOptions) metrics.Timer {
	help := strings.TrimSpace(options.Help)
	if len(help) == 0 {
		help = options.Name
	}
	name := d.subScope(options.Name)
	tags := d.mergeTags(options.Tags)
	labelNames := d.tagNames(tags)
	opts := prometheus.HistogramOpts{
		Name:    name,
		Help:    help,
		Buckets: asFloatBuckets(options.Buckets),
	}
	hv := d.cache.getOrMakeHistogramVec(opts, labelNames)
	return &timer{
		histogram: hv.WithLabelValues(d.tagsAsLabelValues(labelNames, tags)...),
	}
}

func asFloatBuckets(buckets []time.Duration) []float64 {
	data := make([]float64, len(buckets))
	for i := range data {
		data[i] = float64(buckets[i]) / float64(time.Second)
	}
	return data
}

// Histogram implements Histogram of metrics.Data.
func (d *Data) Histogram(options metrics.HistogramOptions) metrics.Histogram {
	help := strings.TrimSpace(options.Help)
	if len(help) == 0 {
		help = options.Name
	}
	name := d.subScope(options.Name)
	tags := d.mergeTags(options.Tags)
	labelNames := d.tagNames(tags)
	opts := prometheus.HistogramOpts{
		Name:    name,
		Help:    help,
		Buckets: options.Buckets,
	}
	hv := d.cache.getOrMakeHistogramVec(opts, labelNames)
	return &histogram{
		histogram: hv.WithLabelValues(d.tagsAsLabelValues(labelNames, tags)...),
	}
}

// Namespace implements Namespace of metrics.Data.
func (d *Data) Namespace(scope metrics.NSOptions) metrics.Data {
	return newData(d, d.subScope(scope.Name), d.mergeTags(scope.Tags))
}

type counter struct {
	counter prometheus.Counter
}

func (c *counter) Inc(v int64) {
	c.counter.Add(float64(v))
}

type gauge struct {
	gauge prometheus.Gauge
}

func (g *gauge) Update(v int64) {
	g.gauge.Set(float64(v))
}

type observer interface {
	Observe(v float64)
}

type timer struct {
	histogram observer
}

func (t *timer) Record(v time.Duration) {
	t.histogram.Observe(float64(v.Nanoseconds()) / float64(time.Second/time.Nanosecond))
}

type histogram struct {
	histogram observer
}

func (h *histogram) Record(v float64) {
	h.histogram.Observe(v)
}

func (d *Data) subScope(name string) string {
	if d.scope == "" {
		return d.normalize(name)
	}
	if name == "" {
		return d.normalize(d.scope)
	}
	return d.normalize(d.scope + string(d.separator) + name)
}

func (d *Data) normalize(v string) string {
	return d.normalizer.Replace(v)
}

func (d *Data) mergeTags(tags map[string]string) map[string]string {
	ret := make(map[string]string, len(d.tags)+len(tags))
	for k, v := range d.tags {
		ret[k] = v
	}
	for k, v := range tags {
		ret[k] = v
	}
	return ret
}

func (d *Data) tagNames(tags map[string]string) []string {
	ret := make([]string, 0, len(tags))
	for k := range tags {
		ret = append(ret, k)
	}
	sort.Strings(ret)
	return ret
}

func (d *Data) tagsAsLabelValues(labels []string, tags map[string]string) []string {
	ret := make([]string, 0, len(tags))
	for _, l := range labels {
		ret = append(ret, tags[l])
	}
	return ret
}

func counterNamingConvention(name string) string {
	if !strings.HasSuffix(name, "_total") {
		name += "_total"
	}
	return name
}
